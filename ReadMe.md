# README

## Notes to reviewer
- In this project, I have tried to demonestrate design & development skills and best practices as possible using the given problem.

- Reviewing **commit-by-commit** is recommended to give you a better idea on the thought process and the progress that resulted in this final outcome.
  I have intentially followed the problem described phase by phase, to show how the design can evolve from initial requirements to Extension 1 and Extension 2.

- Design patterns and best practices has been applied reasonably to fit the problem and to show understanding of principles.
    - Adaptor pattern to wrap the implementation details of search engine integration
    - Factory Method pattern to generate different search ranking adaptors
    - SOLID priciples applied across implementation
    - Immutable types when needed, like the DTO `SearchResult`

- Last but not the least, I chose to use a Web UI instead of a console app to challenge myself building my first-ever Reactjs app. I have learnt it for two days literally and I decided to use it although I prefer working on backend most of the time but I'm open to help when needed. It was an exciting experiment but please don't rate this or compare it to usual frontend developers, it's just a starter. There's a room for enhancement definitely, at least in testing and splitting components into separate files.

## Where to find code
- To review commit-by-commit, use to https://bitbucket.org/waheedsayed/searchranking/commits/
- To review source code, use to https://bitbucket.org/waheedsayed/searchranking/src/master/src/

## How to run the project

Execute the following command after cloning the repository to build solution
```
dotnet run -p src/Sympli.SearchRanking/Sympli.SearchRanking.csproj
```
Then, head to https://localhost:5001/


![screenshot](Screenshot.png "Final outcome")