using FluentAssertions;
using NUnit.Framework;
using Sympli.SearchRanking.Extensions;

namespace Sympli.SearchRanking.Tests.Extensions
{
    [TestFixture]
    public class StringExtensionsTests
    {
        [TestCase(null)]
        [TestCase("")]
        [TestCase("   ")]
        public void IsNullOrEmpty_WhenInputIsNullOrEmpty_ReturnsTrue(string input)
        {
            var result = input.IsNullOrEmpty();
            result.Should().BeTrue();
        }
        [TestCase("abc")]
        [TestCase("123")]
        [TestCase("@")]
        public void IsNullOrEmpty_WhenInputHasValue_ReturnsFalse(string input)
        {
            var result = input.IsNullOrEmpty();
            result.Should().BeFalse();
        }

    }
}
