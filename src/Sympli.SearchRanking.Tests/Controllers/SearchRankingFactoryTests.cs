using System.Net.Http;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;
using Sympli.SearchRanking.Controllers;
using Sympli.SearchRanking.Logic;
using Sympli.SearchRanking.Logic.Bing;
using Sympli.SearchRanking.Logic.DuckDuckGo;
using Sympli.SearchRanking.Logic.Google;
using Sympli.SearchRanking.Logic.Yahoo;

namespace Sympli.SearchRanking.Tests.Controllers
{
    [TestFixture]
    public class SearchRankingFactoryTests
    {
        private readonly IHttpClientFactory _httpClientFactory = Mock.Of<IHttpClientFactory>();
        private readonly IOptions<GoogleSearchOptions> _googleSearchOptions = Mock.Of<IOptions<GoogleSearchOptions>>();
        private readonly IOptions<BingSearchOptions> _bingSearchOptions = Mock.Of<IOptions<BingSearchOptions>>();
        private readonly IOptions<DuckDuckGoSearchOptions> _duckduckgoSearchOptions = Mock.Of<IOptions<DuckDuckGoSearchOptions>>();
        private readonly IOptions<YahooSearchOptions> _yahooSearchOptions = Mock.Of<IOptions<YahooSearchOptions>>();

        private readonly ISearchRankingFactory _searchRankingFactory;

        public SearchRankingFactoryTests()
        {
            _searchRankingFactory = new SearchRankingFactory(
                _httpClientFactory,_googleSearchOptions, _bingSearchOptions, _duckduckgoSearchOptions, _yahooSearchOptions);
        }

        public void CanCreateInstanceOfGoogleSearchRank()
        {
            var result = _searchRankingFactory.Create(SearchEngine.Bing);

            result.Should().BeOfType<ISearchRank>().And.BeOfType<GoogleSearchRank>();
        }

    }
}
