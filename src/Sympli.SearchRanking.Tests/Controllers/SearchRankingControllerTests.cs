using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using Sympli.SearchRanking.Controllers;
using Sympli.SearchRanking.Logic;
using static Sympli.SearchRanking.Constants;

namespace Sympli.SearchRanking.Tests.Controllers
{
    public class SearchRankingControllerTests
    {
        private readonly IFixture _fixture = new Fixture();

        private SearchRankingController _subject;
        private ISearchRankingFactory _searchRankingFactory;
        private ISearchResultCache _searchResultCache;

        [SetUp]
        public void Setup()
        {
            _searchRankingFactory = Mock.Of<ISearchRankingFactory>();
            _searchResultCache = Mock.Of<ISearchResultCache>();
            _subject = new SearchRankingController(_searchRankingFactory, _searchResultCache);
        }

        [TearDown]
        public void TearDown()
        {
            Mock.Get(_searchRankingFactory).Verify();
            Mock.Get(_searchResultCache).Verify();
        }

        [TestCase(null, "https://some-url")]
        [TestCase("some-keywords", null)]
        public async Task Get_WhenKeywordsOrUrlIsMissing_ReturnsBadRequest(string keywords, string url)
        {
            var result = await _subject.Get(keywords, url);
            result.Should().BeOfType<BadRequestObjectResult>()
                .Subject.Value.Should().Be(ErrorMessage.KeywordsAndUrlAreRequired);
        }

        [Test]
        public async Task Get_WhenSearchRankIsNotCached_ReturnsFreshResult()
        {
            var keywords = _fixture.Create<string>();
            var url = _fixture.Create<string>();
            var expectedResults = new List<SearchResult>();

            foreach (var engine in Enum.GetValues<SearchEngine>())
            {
                var result = _fixture.Create<string>();
                var cacheKey = _fixture.Create<string>();
                var searchResult = new SearchResult(engine.ToString(), result);
                expectedResults.Add(searchResult);

                Mock.Get(_searchRankingFactory)
                    .Setup(x => x.Create(engine).FindUrlRankAsync(keywords, url))
                    .ReturnsAsync(result)
                    .Verifiable();

                Mock.Get(_searchResultCache)
                    .Setup(x => x.GetCacheKey(engine, keywords, url))
                    .Returns(cacheKey)
                    .Verifiable();

                Mock.Get(_searchResultCache)
                    .Setup(x => x.GetOrCreateAsync(engine, cacheKey, It.IsAny<Func<Task<string>>>()))
                    .Callback(() => _searchRankingFactory.Create(engine).FindUrlRankAsync(keywords, url))
                    .ReturnsAsync(searchResult)
                    .Verifiable();
            }

            var actual = await _subject.Get(keywords, url);

            actual.Should().BeOfType<OkObjectResult>()
                .Which.StatusCode.Should().Be((int)HttpStatusCode.OK);

            actual.Should().BeOfType<OkObjectResult>()
                .Which.Value.Should().BeEquivalentTo(expectedResults, options => options.WithStrictOrdering()); // Because test called only Google.
        }

        [Test]
        public async Task Get_WhenSearchRankIsCached_ReturnsCached()
        {
            var keywords = _fixture.Create<string>();
            var url = _fixture.Create<string>();
            var expectedResults = new List<SearchResult>();

            foreach (var engine in Enum.GetValues<SearchEngine>())
            {
                var result = _fixture.Create<string>();
                var cacheKey = _fixture.Create<string>();
                var searchResult = new SearchResult(engine.ToString(), result);
                expectedResults.Add(searchResult);

                Mock.Get(_searchResultCache)
                    .Setup(x => x.GetCacheKey(engine, keywords, url))
                    .Returns(cacheKey)
                    .Verifiable();

                Mock.Get(_searchResultCache)
                    .Setup(x => x.GetOrCreateAsync(engine, cacheKey, It.IsAny<Func<Task<string>>>()))
                    .ReturnsAsync(searchResult)
                    .Verifiable();
            }

            var actual = await _subject.Get(keywords, url);

            actual.Should().BeOfType<OkObjectResult>()
                .Which.StatusCode.Should().Be((int)HttpStatusCode.OK);

            actual.Should().BeOfType<OkObjectResult>()
                .Which.Value.Should().BeEquivalentTo(expectedResults, options => options.WithStrictOrdering()); // Because test called only Google.
        }
    }
}
