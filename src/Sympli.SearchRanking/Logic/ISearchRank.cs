using System.Threading.Tasks;

namespace Sympli.SearchRanking.Logic
{
    public interface ISearchRank
    {
        /// <summary>
        /// Returns the rank of a given <c>Url</c> when searching with given <c>keywords</c> concatenated as a string.
        /// </summary>
        /// <remarks>
        /// By the rank we mean the order of where this URL appears in search results.
        /// Given that we care only about the first 100 results.
        /// </remarks>
        /// <param name="keywords">Keywords for search, e.g. "e-settlements"</param>
        /// <param name="url">A Url that we desire to find its rank, e.g. www.sympli.com.au</param>
        /// <returns>A string representation of the search outcome, e.g. "1,10,33"</returns>
        Task<string> FindUrlRankAsync(string keywords, string url);
    }
}
