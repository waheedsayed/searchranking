using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;

namespace Sympli.SearchRanking.Logic.DuckDuckGo
{
    public class DuckDuckGoSearchRank : ISearchRank
    {
        // ReSharper disable NotAccessedField.Local
        // This should be use to call DuckDuckGo Search API but we're not going to do so in this example as requested.
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly string _engineUrl;
        private readonly string _apiKey;
        // ReSharper enable NotAccessedField.Local

        public DuckDuckGoSearchRank(IHttpClientFactory httpClientFactory, IOptions<DuckDuckGoSearchOptions> options)
        {
            _httpClientFactory = httpClientFactory;
            _engineUrl = options.Value.EngineUrl;
            _apiKey = options.Value.ApiKey;
        }

        public Task<string> FindUrlRankAsync(string keywords, string url)
        {
            // 1) Fetching results from search engine
            // 2) Transform results to a string represents the rank in search result
            return Task.FromResult("2,3,4");
        }
    }
}
