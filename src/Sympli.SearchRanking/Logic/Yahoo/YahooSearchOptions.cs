using System.ComponentModel.DataAnnotations;

namespace Sympli.SearchRanking.Logic.Yahoo
{
    /// <summary>
    /// Yahoo Search API options
    /// </summary>
    public class YahooSearchOptions
    {
        /// <summary>
        /// Section name in configuration files
        /// </summary>
        public const string SectionName = "YahooSearchOptions";

        /// <summary>
        /// URL for search engine to run a query
        /// </summary>
        [Required]
        public string EngineUrl { get; set; }

        /// <summary>
        /// API Key to query a search engine.
        /// Assumption: It's always required to have an API key to execute such request.
        /// </summary>
        [Required]
        public string ApiKey { get; set; }

    }
}
