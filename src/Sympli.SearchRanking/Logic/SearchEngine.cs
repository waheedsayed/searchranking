namespace Sympli.SearchRanking.Logic
{
    /// <summary>
    /// List of interesting search engines
    /// check: https://gs.statcounter.com/search-engine-market-share/all/australia
    /// </summary>
    public enum SearchEngine
    {
        Google,
        Bing,
        DuckDuckGo,
        Yahoo
    }
}
