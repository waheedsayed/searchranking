namespace Sympli.SearchRanking
{
    public static class Constants
    {
        public static class ErrorMessage
        {
            public const string KeywordsAndUrlAreRequired = "keywords and url are required.";
        }
    }
}
