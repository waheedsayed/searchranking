import React, {useState} from "react";
import {Badge, Button, Col, Container, Form, FormGroup, Input, Label, Row} from "reactstrap";
import {Card, CardBody, CardTitle, CardSubtitle, CardText} from "reactstrap";

function SearchRanking() {
  const emptyResult = {engine:null, result:null, timestamp:null};
  const defaultKeywords = "e-settlements";
  const defaultUrl = "www.sympli.com.au";
  const [keywords, setKeywords] = useState(defaultKeywords);
  const [url, setUrl] = useState(defaultUrl);
  const [results, setResults] = useState({list: [emptyResult], loading: true});

  const getSearchRanking = async () => {
    const response = await fetch(`api/search-ranking?keywords=${keywords}&url=${url}`);
    const data = await response.json();
    console.log(data);
    setResults({list: data, loading: false});
  };

  const resetForm = () => {
    setKeywords(defaultKeywords);
    setUrl(defaultUrl);
    setResults({list: [emptyResult], loading: true});
  }

  return (
    <Container>
      <Row>
        <Col>
          <SearchForm
            submitFunction={getSearchRanking}
            resetFunction={resetForm}
            keywordsValue={keywords}
            urlValue={url}
            setKeywords={setKeywords}
            setUrl={setUrl} />
        </Col>
      </Row>
      <Row>
        <Col>
          <SearchResultGrid resultsValue={results} />
        </Col>
      </Row>
    </Container>
  );
}

function SearchForm(props) {
  const handleKeywordsChange = (event) => props.setKeywords(event.target.value);
  const handleUrlChange = (event) => props.setUrl(event.target.value);
  return (
    <div>
      <Card>
        <CardBody>
          <Form>
            <FormGroup>
              <Label for="keywords">Keywords</Label>
              <Input type="text" name="txtKeywords" value={props.keywordsValue} onChange={handleKeywordsChange} />
            </FormGroup>
            <FormGroup>
              <Label for="url">URL</Label>
              <Input type="text" name="txtUrl" value={props.urlValue} onChange={handleUrlChange} />
            </FormGroup>
            <FormGroup>
              <Button outline color="primary" onClick={props.submitFunction}>Submit</Button>
              <Button outline color="warning" onClick={props.resetFunction}>Reset</Button>
            </FormGroup>
          </Form>
          </CardBody>
      </Card>
    </div>
  );
}

function SearchResultGrid(props) {
  if (props.resultsValue.loading) {
    return null;
  }
  return props.resultsValue.list.map(r => <SearchResult key={r.engine} resultValue={r} />)
}

function SearchResult(props) {
  const timestamp = "Last updated: " + new Date(props.resultValue.timestamp).toTimeString();
  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle tag="h5">{props.resultValue.engine}</CardTitle>
          <CardSubtitle tag="h6" className="mb-2 text-muted">Rank in search</CardSubtitle>
          <CardText>{props.resultValue.result}</CardText>
          <CardText><Badge className="text-right" variant="dark">{timestamp}</Badge></CardText>
        </CardBody>
      </Card>
    </div>
  );
}

export default SearchRanking;
