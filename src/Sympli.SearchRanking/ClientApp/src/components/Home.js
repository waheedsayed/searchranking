import React, { Component } from 'react';
import SearchRanking from "./SearchRanking";

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return <SearchRanking />;
  }
}
