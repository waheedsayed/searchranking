using System;
using System.Net.Http;
using Microsoft.Extensions.Options;
using Sympli.SearchRanking.Logic;
using Sympli.SearchRanking.Logic.Bing;
using Sympli.SearchRanking.Logic.DuckDuckGo;
using Sympli.SearchRanking.Logic.Google;
using Sympli.SearchRanking.Logic.Yahoo;

namespace Sympli.SearchRanking.Controllers
{
    /// <summary>
    /// A factory to create different search engine adaptors.
    /// </summary>
    public interface ISearchRankingFactory
    {
        /// <summary>
        /// Returns a search rank adaptor based on the selected search engine.
        /// </summary>
        /// <param name="engine">Selected search engine</param>
        /// <returns>
        /// A concrete adaptor of search rank that implements <see cref="ISearchRank"/>.
        ///
        /// <seealso cref="GoogleSearchRank"/>
        /// <seealso cref="BingSearchRank"/>
        /// <seealso cref="DuckDuckGoSearchRank"/>
        /// <seealso cref="YahooSearchRank"/>
        /// </returns>
        ISearchRank Create(SearchEngine engine);
    }

    public class SearchRankingFactory : ISearchRankingFactory
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IOptions<GoogleSearchOptions> _googleOptions;
        private readonly IOptions<BingSearchOptions> _bingOptions;
        private readonly IOptions<DuckDuckGoSearchOptions> _duckduckgoOptions;
        private readonly IOptions<YahooSearchOptions> _yahooOptions;

        public SearchRankingFactory(
            IHttpClientFactory httpClientFactory,
            IOptions<GoogleSearchOptions> googleOptions,
            IOptions<BingSearchOptions> bingOptions,
            IOptions<DuckDuckGoSearchOptions> duckduckgoOptions,
            IOptions<YahooSearchOptions> yahooOptions)
        {
            _httpClientFactory = httpClientFactory;
            _googleOptions = googleOptions;
            _bingOptions = bingOptions;
            _duckduckgoOptions = duckduckgoOptions;
            _yahooOptions = yahooOptions;
        }
        public ISearchRank Create(SearchEngine engine)
        {
            return engine switch
            {
                SearchEngine.Google => new GoogleSearchRank(_httpClientFactory, _googleOptions),
                SearchEngine.Bing => new BingSearchRank(_httpClientFactory, _bingOptions),
                SearchEngine.DuckDuckGo => new DuckDuckGoSearchRank(_httpClientFactory, _duckduckgoOptions),
                SearchEngine.Yahoo => new YahooSearchRank(_httpClientFactory, _yahooOptions),
                _ => throw new ArgumentOutOfRangeException(nameof(engine), engine, null)
            };
        }
    }
}
