using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sympli.SearchRanking.Logic;
using Sympli.SearchRanking.Extensions;

using static Sympli.SearchRanking.Constants;

namespace Sympli.SearchRanking.Controllers
{
    [ApiController]
    [Route("api/search-ranking")]
    public class SearchRankingController : ControllerBase
    {
        private readonly ISearchRankingFactory _searchRankingFactory;
        private readonly ISearchResultCache _searchResultCache;

        public SearchRankingController(ISearchRankingFactory searchRankingFactory, ISearchResultCache searchResultCache)
        {
            _searchRankingFactory = searchRankingFactory;
            _searchResultCache = searchResultCache;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(SearchResult))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Get([FromQuery] string keywords, [FromQuery] string url)
        {
            if (keywords.IsNullOrEmpty() || url.IsNullOrEmpty())
            {
                return new BadRequestObjectResult(ErrorMessage.KeywordsAndUrlAreRequired);
            }

            var result = new List<SearchResult>();
            foreach (var engine in Enum.GetValues<SearchEngine>())
            {
                result.Add(await FindUrlSearchRankAsync(engine, keywords, url));
            }
            return Ok(result);
        }

        private async Task<SearchResult> FindUrlSearchRankAsync(SearchEngine engine, string keywords, string url)
        {
            var key = _searchResultCache.GetCacheKey(engine, keywords, url);
            var cacheEntry = await _searchResultCache.GetOrCreateAsync(
                engine, key, () => _searchRankingFactory.Create(engine).FindUrlRankAsync(keywords, url));
            return cacheEntry;
        }
    }
}
