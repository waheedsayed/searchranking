using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Sympli.SearchRanking.Logic;

namespace Sympli.SearchRanking.Controllers
{
    public interface ISearchResultCache
    {
        string GetCacheKey(SearchEngine engine, string keywords, string url);
        Task<SearchResult> GetOrCreateAsync(SearchEngine engine, string key, Func<Task<string>> searchResultFactory);
    }

    public class SearchResultCache : ISearchResultCache
    {
        private readonly IMemoryCache _cache;

        public SearchResultCache(IMemoryCache cache)
        {
            _cache = cache;
        }

        public string GetCacheKey(SearchEngine engine, string keywords, string url) => engine + "|" + keywords + "|" + url;

        public async Task<SearchResult> GetOrCreateAsync(SearchEngine engine, string key, Func<Task<string>> searchResultFactory)
        {
            var cacheEntry = await _cache.GetOrCreateAsync(key, async entry =>
            {
                var result = await searchResultFactory();
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1);
                return new SearchResult(engine.ToString(), result);
            });

            return cacheEntry;
        }
    }
}
