using System;

namespace Sympli.SearchRanking.Controllers
{
    public class SearchResult
    {
        public string Engine { get; }
        public string Result { get; }
        public DateTime Timestamp { get; }

        public SearchResult(string engine, string result)
        {
            Engine = engine;
            Result = result;
            Timestamp = DateTime.Now;
        }
    }
}
