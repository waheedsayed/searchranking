using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Sympli.SearchRanking.Filters
{
    public class UnhandledExceptionFilter: IExceptionFilter
    {
        private readonly ILogger<UnhandledExceptionFilter> _logger;

        public UnhandledExceptionFilter(ILogger<UnhandledExceptionFilter> logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            _logger.LogError(context.Exception.ToString());
            context.Result = new StatusCodeResult((int)HttpStatusCode.InternalServerError);
            context.ExceptionHandled = true;
        }
    }
}
