using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Sympli.SearchRanking.Controllers;
using Sympli.SearchRanking.Filters;
using Sympli.SearchRanking.Logic.Bing;
using Sympli.SearchRanking.Logic.DuckDuckGo;
using Sympli.SearchRanking.Logic.Google;
using Sympli.SearchRanking.Logic.Yahoo;

namespace Sympli.SearchRanking
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Search ranking options
            services.AddOptions<GoogleSearchOptions>()
                .Bind(Configuration.GetSection(GoogleSearchOptions.SectionName))
                .ValidateDataAnnotations();
            services.AddOptions<BingSearchOptions>()
                .Bind(Configuration.GetSection(BingSearchOptions.SectionName))
                .ValidateDataAnnotations();
            services.AddOptions<DuckDuckGoSearchOptions>()
                .Bind(Configuration.GetSection(DuckDuckGoSearchOptions.SectionName))
                .ValidateDataAnnotations();
            services.AddOptions<YahooSearchOptions>()
                .Bind(Configuration.GetSection(YahooSearchOptions.SectionName))
                .ValidateDataAnnotations();

            // Search ranking controller dependencies
            services.AddScoped<ISearchRankingFactory, SearchRankingFactory>();
            services.AddScoped<ISearchResultCache, SearchResultCache>();
            services.AddHttpClient();
            services.AddMemoryCache();

            services.AddControllersWithViews(options => options.Filters.Add<UnhandledExceptionFilter>());

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
